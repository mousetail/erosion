#version 440 core

layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 Normal;
layout (location = 2) in vec3 VertexColor;

layout (location = 4) uniform mat4 modelMatrix;
layout (location = 8) uniform mat4 perspectiveMatrix;

out vec3 normal;
out vec3 vertexColor;

void main()
{
    gl_Position = perspectiveMatrix * modelMatrix * vec4(Position, 1.0);
    normal = (modelMatrix * vec4(Normal, 0)).xyz;
    //gl_Position = vec4(VertexColor, 1.0);
    vertexColor = VertexColor;
}
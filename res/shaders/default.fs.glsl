#version 440 core

out vec4 Color;
layout(location=7) uniform vec4 shade_color;
in vec3 normal;
in vec3 vertexColor;

void main()
{
    //Color = vec4(normal/2 + vec3(0.5, 0.5, 0.5), 1);
    //vec4(1.0f, 0.5f, 0.2f, 1.0f)
    Color = shade_color * vec4(vertexColor, 1) * (
        max(dot(normalize(normal), normalize(vec3(0.8, 0.6, 0.4))) * 0.6,0.0)
        + max(dot(normalize(normal), normalize(vec3(-0.4, 0.3, -0.4))), 0) * 0.2
        + vec4(0.2, 0.2, 0.2, 0.0));
    //Color = vec4(vertexColor, 1);
}
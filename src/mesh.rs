use crate::marching;
use std::io::{BufReader, BufRead};
use std::fs::File;
use crate::marching::{Vertex, Triangle, Vector3};
use std::collections::HashMap;

pub(crate) fn load_obj(filename: String, color: &HashMap<String, Vector3>) -> Vec<marching::Triangle> {
    let file = File::open(filename).unwrap();
    let mut reader = BufReader::new(file);

    let mut positions: Vec<Vector3> = vec![];
    let mut normals: Vec<Vector3> = vec![];
    let mut triangles: Vec<Triangle> = vec![];

    let mut line: String = "".parse().unwrap();
    let mut material: String = "".to_string();

    while {
        line = "".to_string();
        reader.read_line(&mut line).unwrap() != 0
    } {
        if line.chars().next().unwrap() == '#' {
            continue; // This is a comment in the OBJ format
        }


        let words: Vec<&str> = line.strip_suffix('\n').unwrap().split(' ').collect();
        if (words[0] == "mtllib") {
            continue; // Ignore Materials (for now)
        } else if words[0] == "v" {
            positions.push(Vector3 {
                x: words[1].parse::<f32>().unwrap(),
                y: words[2].parse::<f32>().unwrap(),
                z: words[3].parse::<f32>().unwrap(),
            });
        } else if words[0] == "vt" {
            //Ignore UV coordinates (for now)
        } else if words[0] == "vn" {
            normals.push(
                Vector3 {
                    x: words[1].parse::<f32>().unwrap(),
                    y: words[2].parse::<f32>().unwrap(),
                    z: words[3].parse::<f32>().unwrap(),
                }
            );
        } else if words[0] == "f" {
            let vertex_strings = &words[1..];
            let mut vertexes: [Vertex; 3] = [Default::default(); 3];

            for i in 0..3 {
                let string = vertex_strings[i];
                let parts = string.split('/').collect::<Vec<&str>>();

                let position = positions[parts[0].parse::<usize>().unwrap() - 1];
                let normal = normals[parts[2].parse::<usize>().unwrap() - 1];

                vertexes[i] = Vertex {
                    position,
                    normal,
                    color: color[&material],
                };
            }

            triangles.push(
                Triangle {
                    xyz: vertexes
                }
            )
        } else if words[0] == "usemtl" {
            material = words[1].to_string();
            if (!color.contains_key(&material)) {
                panic!("cannot find key \"{}\"", material)
            }
        }
    }

    return triangles;
}
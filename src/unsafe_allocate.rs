pub fn unsafe_allocate<T>() -> Box<T> {
    let grid_box: Box<T>;
    unsafe {
        use std::alloc::{dealloc, Layout, alloc_zeroed};
        let layout = Layout::new::<T>();
        let ptr = alloc_zeroed(layout) as *mut T;
        grid_box = Box::from_raw(ptr);
    }
    return grid_box;
}
mod mesh;

extern crate gl;
extern crate sdl2;

use std::ffi::CString;

extern crate nalgebra as na;

mod marching;
mod shaders;
mod genGrid;
mod unsafe_allocate;
mod clamp;

use gl::types::*;
use crate::marching::{Vector3, Triangle, Vertex, vertex_interp};
use std::thread::sleep;
use std::time::Duration;
use std::path::Display;
use crate::clamp::Clamp;
use std::fs::File;
use std::io::Write;
use rand::thread_rng;
use crate::mesh::load_obj;
use std::collections::HashMap;

const MOVE_STEP: f32 = 0.0025;
const NROF_ITERATIONS: usize = 100;
const NROF_GRASS: usize = genGrid::GRID_SIZE * genGrid::GRID_SIZE * 4;


struct Main {
    terrain_vao: GLuint,
    path_vao: GLuint,
    grass_vao: GLuint,

    terrain_buffer: GLuint,
    path_buffer: GLuint,
    grass_buffer: GLuint,

    nrof_grass: usize,
}

impl Main {
    unsafe fn link_buffer(&mut self, buffer: GLuint, vbo: GLuint, data: *const gl::types::GLvoid, length: usize) {
        gl::BindVertexArray(vbo);
        gl::BindBuffer(gl::ARRAY_BUFFER, buffer);
        gl::BufferData(
            gl::ARRAY_BUFFER,
            length as GLsizeiptr, //(triangle_index * std::mem::size_of::<Triangle>()) as gl::types::GLsizeiptr,
            data, //triangles.as_ptr() as *const gl::types::GLvoid,//triangles.0.as_ptr() as *const gl::types::GLvoid,
            gl::STATIC_DRAW);

        // assert_eq!(std::ptr::read(triangles.0.as_ptr() as *const [f32; 9]),
        //            [
        //                triangles.0[0].xyz[0].x,
        //                triangles.0[0].xyz[0].y,
        //                triangles.0[0].xyz[0].z,
        //                triangles.0[0].xyz[1].x,
        //                triangles.0[0].xyz[1].y,
        //                triangles.0[0].xyz[1].z,
        //                triangles.0[0].xyz[2].x,
        //                triangles.0[0].xyz[2].y,
        //                triangles.0[0].xyz[2].z
        //            ]
        // );

        gl::EnableVertexAttribArray(0);
        gl::EnableVertexAttribArray(1);
        gl::EnableVertexAttribArray(2);
        gl::VertexAttribPointer(
            0, // index of the generic vertex attribute ("layout (location = 0)")
            3, // the number of components per generic vertex attribute
            gl::FLOAT, // data type
            gl::FALSE, // normalized (int-to-float conversion)
            (9 * std::mem::size_of::<f32>()) as gl::types::GLint, // stride (byte offset between consecutive attributes)
            std::ptr::null(), // offset of the first component
        );
        gl::VertexAttribPointer(
            1, // index of the generic vertex attribute ("layout (location = 1)")
            3, // the number of components per generic vertex attribute
            gl::FLOAT, // data type
            gl::FALSE, // normalized (int-to-float conversion)
            (9 * std::mem::size_of::<f32>()) as gl::types::GLint, // stride (byte offset between consecutive attributes)
            (3 * std::mem::size_of::<f32>()) as *const gl::types::GLvoid, // offset of the first component
        );
        gl::VertexAttribPointer(
            2, // index of the generic vertex attribute ("layout (location = 2)")
            3, // the number of components per generic vertex attribute
            gl::FLOAT, // data type
            gl::FALSE, // normalized (int-to-float conversion)
            (9 * std::mem::size_of::<f32>()) as gl::types::GLint, // stride (byte offset between consecutive attributes)
            (6 * std::mem::size_of::<f32>()) as *const gl::types::GLvoid, // offset of the first component
        );
    }

    fn update_mesh(&mut self, grid: &genGrid::Grid, range: &[Vector3; 8], triangle_index: &mut usize, triangles: &mut [Triangle; genGrid::GRID_VOLUME * 5]) {
        *triangle_index = 0;
        for x in 0..(genGrid::GRID_SIZE - 1) {
            for y in 0..(genGrid::GRID_SIZE - 1) {
                for z in 0..(genGrid::GRID_SIZE - 1) {
                    // TODO: make this work
                    let index_increment = marching::polygonize(&grid, *range, x, y, z, 0.1, triangles, *triangle_index, false);
                    *triangle_index += index_increment;
                }
            }
        }

        unsafe {
            self.link_buffer(self.terrain_buffer, self.terrain_vao, triangles.as_ptr() as *const GLvoid, *triangle_index * std::mem::size_of::<Triangle>());
            self.link_buffer(self.path_buffer, self.path_vao, grid.last_drop_path.as_ptr() as *const GLvoid, grid.last_drop_length * std::mem::size_of::<Vertex>());
        }
    }

    fn run(&mut self) {
        println!("about to start");

        let mut material_map = HashMap::<String, marching::Vector3>::new();
        material_map.insert("FlowerStalk.001".to_string(), Vector3 { x: 0.375, y: 0.8, z: 0.108 });
        material_map.insert("Material.001".to_string(), Vector3 { x: 0.375, y: 0.8, z: 0.108 });
        material_map.insert("".to_string(), Vector3 { x: 0.375, y: 0.8, z: 0.108 });
        material_map.insert("Blossoms.001".to_string(), Vector3 { x: 0.8, y: 0.8, z: 0.8 });
        material_map.insert("Core.001".to_string(), Vector3 { x: 0.799, y: 0.651, z: 0.159 });
        material_map.insert("FlowerStalk".to_string(), Vector3 { x: 0.475, y: 0.8, z: 0.221 });
        material_map.insert("Core".to_string(), Vector3 { x: 0.8, y: 0.594, z: 0.043 });
        material_map.insert("Blossoms".to_string(), Vector3 { x: 0.8, y: 0.731, z: 0.160 });
        material_map.insert("Blossoms.002".to_string(), Vector3 { x: 0.325, y: 0.023, z: 0.8 });
        material_map.insert("Core.002".to_string(), Vector3 { x: 0.8, y: 0.594, z: 0.043 });
        material_map.insert("Wood".to_string(), Vector3 { x: 0.76, y: 0.6, z: 0.043 });

        let sdl = sdl2::init().unwrap();
        let video_subsystem = sdl.video().unwrap();
        let mut window = video_subsystem
            .window("Game", 1200, 900)
            .opengl()
            .resizable()
            .build()
            .unwrap();

        let mut event_pump = sdl.event_pump().unwrap();
        let mouse = sdl.mouse();

        let gl_context = window.gl_create_context().unwrap();
        let gl = gl::load_with(|s| video_subsystem.gl_get_proc_address(s) as *const std::os::raw::c_void);

        let tree_mesh = load_obj("res/models/tree.obj".to_string(), &material_map);
        let flower_mesh_1 = load_obj("res/models/flower.obj".to_string(), &material_map);
        let flower_mesh_2 = load_obj("res/models/flower2.obj".to_string(), &material_map);
        let flower_mesh_3 = load_obj("res/models/flower3.obj".to_string(), &material_map);
        let grass_mesh = load_obj("res/models/grass.obj".to_string(), &material_map);

        unsafe {
            gl::ClearColor(0.3, 0.3, 0.5, 1.0);
            gl::Enable(gl::CULL_FACE);
            gl::Enable(gl::DEPTH_TEST);
            gl::LineWidth(4.0);
            gl::PointSize(16.0);
        }

        let mut running = true;

        println!("about to create grid");
        let mut grid = genGrid::Grid::new();
        let mut triangles = unsafe_allocate::unsafe_allocate::<[Triangle; genGrid::GRID_VOLUME * 5]>();

        let vert_shader = shaders::Shader::from_vert_source(
            &CString::new(include_str!("..\\res\\shaders\\default.vs.glsl")).unwrap()
        ).unwrap();

        let frag_shader = shaders::Shader::from_frag_source(
            &CString::new(include_str!("..\\res\\shaders\\default.fs.glsl")).unwrap()
        ).unwrap();


        let shader_program = shaders::Program::from_shaders(&[vert_shader, frag_shader]).unwrap();
        shader_program.set_used();


        unsafe {
            gl::GenBuffers(1, &mut self.terrain_buffer);
            gl::GenBuffers(1, &mut self.path_buffer);
            gl::GenBuffers(1, &mut self.grass_buffer);

            gl::GenVertexArrays(1, &mut self.terrain_vao);
            gl::GenVertexArrays(1, &mut self.path_vao);
            gl::GenVertexArrays(1, &mut self.grass_vao);

            //gl::EnableClientState(GL_VERTEX_ARRAY);
        }

        let mut framenum = 0;

        let range = [
            Vector3 { x: -1.0, y: -1.0, z: -1.0 },
            Vector3 { x: -1.0, y: -1.0, z: 1.0 },
            Vector3 { x: -1.0, y: 1.0, z: -1.0 },
            Vector3 { x: -1.0, y: 1.0, z: 1.0 },
            Vector3 { x: 1.0, y: -1.0, z: -1.0 },
            Vector3 { x: 1.0, y: -1.0, z: 1.0 },
            Vector3 { x: 1.0, y: 1.0, z: -1.0 },
            Vector3 { x: 1.0, y: 1.0, z: 1.0 },
        ];


        let mut angle: f32 = std::f32::consts::FRAC_PI_4;
        let mut pitch: f32 = std::f32::consts::FRAC_PI_4;

        let mut camera_position = na::Point3::from([0.0, 0.0, 0.0]);

        let mut model_view_matrix =
            na::Matrix4::look_at_rh(
                &na::Point::from([1.5, 1.5, 0.0]),
                &na::Point::from([0.0, 0.0, 0.0]),
                &na::Vector3::from([0.0, 1.0, 0.0]));
        let projection_matrix = na::Matrix4::new_perspective(900.0 / 700.0, 45.0, 0.01, 5.0);


        let mut triangle_index = 0;
        let mut first_person_mode = false;

        self.update_mesh(&grid, &range, &mut triangle_index, &mut triangles);


        while running {
            framenum += 1;
            let events: Vec<sdl2::event::Event> = event_pump.poll_iter().collect();
            for event in events {
                match event {
                    sdl2::event::Event::Quit { .. } => running = false,
                    sdl2::event::Event::MouseButtonDown { .. } => {}
                    sdl2::event::Event::MouseMotion { mousestate, x, y, xrel, yrel, .. } => {
                        if mousestate.left() || first_person_mode {
                            let multiplyer = if first_person_mode { 1.0 } else { 1.0 };

                            pitch += yrel as f32 / 80.0 * multiplyer;
                            pitch = pitch.clamp(-std::f32::consts::FRAC_PI_2 + 0.1, std::f32::consts::FRAC_PI_2 - 0.1);
                            angle += xrel as f32 / 80.0 * multiplyer;
                        }
                    }
                    sdl2::event::Event::Window {
                        win_event, ..
                    } => {
                        match (win_event) {
                            sdl2::event::WindowEvent::Resized(x, y) => {
                                let old_display_mode = window.display_mode().unwrap();
                                window.set_display_mode(sdl2::video::DisplayMode {
                                    w: x / 2,
                                    h: y,
                                    format: old_display_mode.format,
                                    refresh_rate: old_display_mode.refresh_rate,
                                }).unwrap();
                            }
                            _ => {}
                        }
                    }
                    sdl2::event::Event::KeyDown { keycode, .. } => {
                        let code = keycode.unwrap();
                        match code {
                            sdl2::keyboard::Keycode::Space => {
                                let start_time = std::time::Instant::now();

                                for i in 0..NROF_ITERATIONS {
                                    grid.water_drop();
                                }

                                let mid_time = std::time::Instant::now();
                                println!("1. elapsed time: {}", (mid_time - start_time).as_millis());

                                self.update_mesh(&grid, &range, &mut triangle_index, &mut triangles);

                                let end_time = std::time::Instant::now();
                                println!("2. elapsed time: {}", (end_time - mid_time).as_millis());
                                println!("   Iteration: {}", grid.last_drop_length);
                                //println!("length: {}, first item: {}", grid.last_drop_length, grid.last_drop_path[0])
                            }
                            sdl2::keyboard::Keycode::Return => {
                                grid.water_drop();
                                self.update_mesh(&grid, &range, &mut triangle_index, &mut triangles);
                            }
                            sdl2::keyboard::Keycode::Insert => {
                                let mut file = File::create("output.dat").unwrap();
                                for i in 0..genGrid::GRID_SIZE {
                                    for j in 0..genGrid::GRID_SIZE {
                                        for k in 0..genGrid::GRID_SIZE {
                                            file.write_all(grid.weights[i][j][k].to_string().as_bytes()).unwrap();
                                            file.write_all("\n".as_bytes()).unwrap();
                                        }
                                    }
                                }
                            }
                            sdl2::keyboard::Keycode::C => {
                                grid.clean();

                                self.update_mesh(&grid, &range, &mut triangle_index, &mut triangles);
                            }
                            sdl2::keyboard::Keycode::G => {
                                let random = thread_rng();

                                let start_time = std::time::Instant::now();
                                let mut nrof_particles_generated = 0;

                                let mut valid_grass_triangles: Vec<usize> = Vec::new();
                                for i in 0..triangle_index {
                                    if triangles[i].get_walkable() && triangles[i].xyz[0].color.x < 0.5 {
                                        valid_grass_triangles.push(i);
                                    }
                                }

                                if (valid_grass_triangles.len() == 0) {
                                    println!("ERROR: no valid particles found");
                                    return;
                                }

                                let mid_time = std::time::Instant::now();
                                println!("time to mid took {}ms", (mid_time - start_time).as_millis());

                                let mut triangle_buffer = unsafe_allocate::unsafe_allocate::<[Vertex; NROF_GRASS * 200]>();
                                let mut grass_index = 0;
                                for i in 0..NROF_GRASS {
                                    let triangle_for_grass = valid_grass_triangles[rand::random::<usize>() % valid_grass_triangles.len()];

                                    let position = vertex_interp(
                                        rand::random::<f32>(), triangles[triangle_for_grass].xyz[0].position,
                                        vertex_interp(
                                            rand::random::<f32>(),
                                            triangles[triangle_for_grass].xyz[1].position,
                                            triangles[triangle_for_grass].xyz[2].position,
                                            0.0,
                                            1.0),
                                        0.0,
                                        1.0);

                                    let angle = rand::random::<f32>() * std::f32::consts::PI;

                                    let random_value = rand::random::<f32>();
                                    let mesh = if random_value < 0.075 {
                                        &tree_mesh
                                    } else if random_value < 0.15 {
                                        &flower_mesh_1
                                    } else if random_value < 0.25 {
                                        &flower_mesh_2
                                    } else if random_value < 0.35 {
                                        &flower_mesh_3
                                    } else {
                                        &grass_mesh
                                    };
                                    for triangle in mesh.iter() {
                                        for point in triangle.xyz.iter() {
                                            triangle_buffer[grass_index] = point.rotate(angle, marching::Vector3::up()) * (1.0 / 8.0) + position;
                                            grass_index += 1;
                                        }
                                    }

                                    nrof_particles_generated += 1;

                                    if grass_index >= triangle_buffer.len() - flower_mesh_2.len() * 3 {
                                        println!("exited because used too many polygons");
                                        break;
                                    }
                                }
                                unsafe {
                                    self.link_buffer(self.grass_buffer, self.grass_vao, triangle_buffer.as_ptr() as *const GLvoid,
                                                     grass_index * std::mem::size_of::<marching::Vertex>(),
                                    );
                                }
                                self.nrof_grass = grass_index;
                                let end_time = std::time::Instant::now();

                                println!(" Generated {} Grass Particles, ({} tris) took {}ms", nrof_particles_generated, grass_index / 3, (end_time - start_time).as_millis());
                            }
                            sdl2::keyboard::Keycode::F => {
                                first_person_mode = !first_person_mode;
                                window.set_grab(first_person_mode);
                                if (first_person_mode) {
                                    camera_position = na::Point::from([3.0 * angle.cos() * pitch.cos(), 3.0 * pitch.sin(), 3.0 * angle.sin() * pitch.cos()]);
                                    mouse.set_relative_mouse_mode(true);
                                    mouse.show_cursor(false);
                                    //angle = angle - std::f32::consts::PI;
                                } else {
                                    mouse.show_cursor(true);
                                    mouse.set_relative_mouse_mode(false);
                                }
                            }
                            _ => {}
                        }
                    }
                    _ => {}
                }
            }

            unsafe {
                gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
            }

            let keystate = event_pump.keyboard_state();
            if keystate.is_scancode_pressed(sdl2::keyboard::Scancode::W) {

                //Move forwards
                camera_position -= na::Vector3::from([angle.cos() * pitch.cos(), pitch.sin(), angle.sin() * pitch.cos()]) * MOVE_STEP;
            } else if keystate.is_scancode_pressed(sdl2::keyboard::Scancode::S) {

                //Move backwards
                camera_position += na::Vector3::from([angle.cos() * pitch.cos(), pitch.sin(), angle.sin() * pitch.cos()]) * MOVE_STEP;
            } else if keystate.is_scancode_pressed(sdl2::keyboard::Scancode::A) {
                camera_position -= na::Vector3::from([angle.sin(), 0.0, -angle.cos()]) * MOVE_STEP;
                //Move Left
            } else if keystate.is_scancode_pressed(sdl2::keyboard::Scancode::D) {
                camera_position += na::Vector3::from([angle.sin(), 0.0, -angle.cos()]) * MOVE_STEP;
                //Move right
            }

            if (!first_person_mode) {
                model_view_matrix =
                    na::Matrix4::look_at_rh(
                        &na::Point::from([3.0 * angle.cos() * pitch.cos(), 3.0 * pitch.sin(), 3.0 * angle.sin() * pitch.cos()]),
                        &na::Point::from([0.0, 0.0, 0.0]),
                        &na::Vector3::from([0.0, 1.0, 0.0]));
            } else {
                model_view_matrix =
                    na::Matrix4::look_at_rh(
                        &camera_position,
                        &(camera_position.clone() - na::Vector::from([angle.cos() * pitch.cos(), pitch.sin(), angle.sin() * pitch.cos()])),
                        &na::Vector3::from([0.0, 1.0, 0.0]));
                na::Matrix::scale_mut(&mut model_view_matrix, 20.0);
            }


            unsafe {
                gl::UniformMatrix4fv(4, 1, 0, model_view_matrix.data.to_vec().as_ptr());
                gl::UniformMatrix4fv(8, 1, 0, projection_matrix.data.as_ptr());


                gl::BindVertexArray(self.terrain_vao);
                gl::Uniform4fv(7, 1, [1.0, 1.0, 1.0, 1.0].as_ptr());

                gl::DrawArrays(
                    gl::TRIANGLES,
                    0,
                    (triangle_index * 3) as gl::types::GLint,
                );


                gl::BindVertexArray(self.path_vao);

                gl::Uniform4fv(7, 1, [1.0, 1.0, 1.0, 1.0].as_ptr());

                gl::DrawArrays(
                    gl::LINE_STRIP,
                    0,
                    grid.last_drop_length as i32,
                );


                gl::Uniform4fv(7, 1, [0.25, 0.25, 0.25, 1.0].as_ptr());


                gl::DrawArrays(
                    gl::POINTS,
                    0,
                    grid.last_drop_length as i32,
                );

                gl::BindVertexArray(self.grass_vao);
                gl::Uniform4fv(7, 1, [1.0, 1.0, 1.0].as_ptr());

                gl::DrawArrays(
                    gl::TRIANGLES,
                    0,
                    self.nrof_grass as i32,
                )
            }

            window.gl_swap_window();
        }
    }
}

fn main() {
    Main {
        terrain_vao: 0,
        path_vao: 0,
        grass_vao: 0,
        terrain_buffer: 0,
        path_buffer: 0,
        grass_buffer: 0,
        nrof_grass: 0,
    }.run();
}

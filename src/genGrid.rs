use rand::prelude::*;
use std::mem::MaybeUninit;
use crate::unsafe_allocate::unsafe_allocate;
use noise::{NoiseFn, Perlin, Seedable, OpenSimplex};
use std::thread::sleep;
use std::time::Duration;
use std::cmp::min;
use crate::marching::{Vertex, Vector3, vertex_interp};
use std::ops::Range;

pub const GRID_SIZE: usize = 64;
pub const GRID_VOLUME: usize = GRID_SIZE * GRID_SIZE * GRID_SIZE;
const SLOPE_STRENGTH: f32 = 0.5;
const DEFAULT_SLOPE: f32 = 0.05;
const EROSION_STRENGTH: f32 = 0.3;
//0.3;
pub const STRENGTH_POWER: f32 = 1.1;
pub const WEAK_EDGE_FACTOR: f32 = 0.8;
pub const WEAK_EDGE_STRENGTH: f32 = 0.25;
/*
struct SpiralIterator {
    size: usize,
    index: usize
}

impl Iterator for SpiralIterator {
    type Item = (usize, usize);

    fn next(&mut self) -> Option<Self::Item> {
        let iteration_number = index.sqrt();

        // Lengths, start at 4
        // 2n + 4
        // n^2 - (n-2)^2 = 4 * n + 4

        return (0,0)
    }
}*/


struct GridCellInfo {
    //Whether water could sit on this spot
    water_safe: bool,
    //Whether this spot is attached to the ground
    ground_safe: bool,
}

pub(crate) struct Grid {
    pub weights: [[[f32; GRID_SIZE]; GRID_SIZE]; GRID_SIZE],
    pub hardness: [[[f32; GRID_SIZE]; GRID_SIZE]; GRID_SIZE],
    pub last_drop_path: [Vertex; GRID_SIZE * 8],
    pub last_drop_strength: [f32; GRID_SIZE * 8],
    pub last_drop_length: usize,
}

struct Gradient {
    x_p: f32,
    y_p: f32,
    z_p: f32,
    x_n: f32,
    y_n: f32,
    z_n: f32,
    value: f32,
}

impl Grid {
    fn get_weights(x: usize, y: usize, z: usize, noise: &dyn NoiseFn<noise::Point3<f64>>) -> f32 {
        if x == 0 || x == GRID_SIZE - 1 || y == 0 || y == GRID_SIZE - 1 || z == 0 || z == GRID_SIZE - 1 {
            return 0.0;
        } else {
            if y >= GRID_SIZE - 1 {
                return 0.0;
            } else if y >= GRID_SIZE - 4 {
                return (x as f32 / GRID_SIZE as f32).min(z as f32 / GRID_SIZE as f32).min(1.0 - x as f32 / GRID_SIZE as f32).min(1.0 - z as f32 / GRID_SIZE as f32);
            }
            return 1.0;
        }

        // let noise_value = noise.get([x as f64 * 0.347, y as f64 * 0.324, z as f64 * 0.282]) as f32 + 0.5;
        //
        // return noise_value;
    }

    pub fn new() -> Box<Grid> {
        use std::alloc::{alloc, dealloc, Layout};

        let mut grid_box: Box<Grid>;

        grid_box = unsafe_allocate::<Grid>();

        let noise = OpenSimplex::new();
        noise.set_seed(4892);

        sleep(Duration::from_secs(1));

        for x in 0..GRID_SIZE {
            for y in 0..GRID_SIZE {
                for z in 0..GRID_SIZE {
                    let (x_i, y_i, z_i) = (x as f32 / (GRID_SIZE - 1) as f32, y as f32 / (GRID_SIZE - 1) as f32, z as f32 / (GRID_SIZE - 1) as f32);
                    let distance = ((x_i * 0.5 - 0.5) * (x_i * 0.5 - 0.5)) + ((y_i - 0.5) * (y_i - 0.5)) + ((z_i - 0.5) * (z_i - 0.5));

                    grid_box.weights[x][y][z] = Grid::get_weights(x, y, z, &noise);

                    let noise_value: f32 = noise.get([x as f64 / 4.2, y as f64 / 3.6, z as f64 / 4.0]) as f32;
                    let distance_from_edge = (
                        x as f32 / (GRID_SIZE - 1) as f32).min(
                        z as f32 / (GRID_SIZE - 1) as f32).min(
                        1.0 - x as f32 / (GRID_SIZE - 1) as f32).min(
                        1.0 - z as f32 / (GRID_SIZE - 1) as f32);
                    grid_box.hardness[x][y][z] = (noise_value * STRENGTH_POWER).exp2() + (4.0 - 4.0 * y as f32 / (GRID_SIZE - 1) as f32);
                    if distance_from_edge <= 0.15 && y as f32 > WEAK_EDGE_FACTOR * GRID_SIZE as f32 {
                        grid_box.hardness[x][y][z] *= distance_from_edge * (1.0 - WEAK_EDGE_STRENGTH) / 0.15 + WEAK_EDGE_STRENGTH;
                    }
                }
            }
        }
        return grid_box;
    }

    fn calculate_gradients(&self, x: usize, y: usize, z: usize) -> Gradient {
        let height = self.weights[x][y][z];
        let h_x_p: f32 = if x < GRID_SIZE - 1 { self.weights[x + 1][y][z] } else { 1.0 };
        let h_x_n: f32 = if x > 0 { self.weights[x - 1][y][z] } else { -1.0 };
        let h_y_p = if y < GRID_SIZE - 1 { self.weights[x][y + 1][z] } else { 1.0 };
        let h_y_n = if y > 0 { self.weights[x][y - 1][z] } else { -1.0 };
        let h_z_p = if z < GRID_SIZE - 1 { self.weights[x][y][z + 1] } else { 1.0 };
        let h_z_n = if z > 0 { self.weights[x][y][z - 1] } else { -1.0 };

        return Gradient {
            x_p: height - h_x_p,
            y_p: height - h_y_p,
            z_p: height - h_z_p,
            x_n: height - h_x_n,
            y_n: height - h_y_n,
            z_n: height - h_z_n,
            value: height,
        };
    }

    fn calculate_range(num: usize) -> Range<usize> {
        let min = (num.max(5) - 4);
        let max = (num + 5).max(1).min(GRID_SIZE - 1);
        return min..max;
    }

    fn gausian_subtract(&mut self, x: f32, y: f32, z: f32, weight: f32) {
        assert!(!weight.is_nan());
        let mut x_1 = x as usize;
        let mut y_1 = y as usize;
        let mut z_1 = z as usize;
        for i in Grid::calculate_range(x_1) {
            for j in Grid::calculate_range(y_1) {
                for k in Grid::calculate_range(z_1) {
                    let distance = (x - i as f32).powi(2) + (y - j as f32).powi(2) + (z - k as f32).powi(2);
                    let delta = 0.05 / (distance + 0.1);
                    self.weights[i][j][k] += delta * weight / self.hardness[i][j][k];
                    assert!(!self.weights[i][j][k].is_nan())
                }
            }
        }
    }

    pub fn calculate_height(&self, x: f32, z: f32) -> f32 {
        let mut y = GRID_SIZE as f32 - 0.01;
        while self.weights[x as usize][y as usize][z as usize] <= 0.0 && y > 0.0 {
            y -= 1.0;
        }
        y += self.weights[x as usize][y as usize][z as usize];
        return y;
    }

    fn calculate_total_path_length(&self, nrof_vertexes: usize) -> f32 {
        let mut length = 0.0;
        for i in 1..nrof_vertexes - 1 {
            length += (self.last_drop_path[i + 1].position - self.last_drop_path[i].position).length();
        }
        return length;
    }

    pub fn water_drop(&mut self) {
        use rand::distributions::{Distribution, Uniform};

        let speed_distribution = Uniform::<f32>::from(-1.0..1.0);
        let mut rng = rand::thread_rng();

        let mut x = (rand::random::<usize>() % (GRID_SIZE - 2)) as f32 + 1.0;
        let mut z = (rand::random::<usize>() % (GRID_SIZE - 2)) as f32 + 1.0;
        let mut y = self.calculate_height(x, z);
        let mut dx = speed_distribution.sample(&mut rng);
        let mut dz = speed_distribution.sample(&mut rng);


        let mut stabalized = false;
        let mut reached_edge = false;

        let mut sediment = 1.0;

        self.last_drop_path[0] = Vertex {
            position: Vector3 {
                x,//: x /// GRID_SIZE as f32 * 2.0 - 1.0,
                y: 2.0 * GRID_SIZE as f32,//: 2.0,
                z,//: z // GRID_SIZE as f32 * 2.0 - 1.0,
            },
            normal: Default::default(),
            color: Default::default(),
        };

        let mut nrof_vertexes = 1;

        while !stabalized {
            let speed = dx * dx + dz * dz;

            let slopes = self.calculate_gradients(x as usize, y as usize, z as usize);
            if slopes.value > 0.9 && slopes.y_p < 0.2 {
                //this means we are inside solid rock
                dx *= 0.6;
                dz *= 0.6;
            } else {
                //Don't actually remove any sediment when underground
            }

            dx *= 0.95;
            dz *= 0.95;

            sediment *= 0.9;


            if dx > 0.0 {
                dx += slopes.x_p * SLOPE_STRENGTH - slopes.x_n * SLOPE_STRENGTH * 0.5;
            } else {
                dx += slopes.x_p * SLOPE_STRENGTH * 0.5 - slopes.x_n * SLOPE_STRENGTH;
            }

            if dz > 0.0 {
                dz += slopes.z_p * SLOPE_STRENGTH - slopes.z_n * SLOPE_STRENGTH * 0.5;
            } else {
                dz += slopes.z_p * SLOPE_STRENGTH * 0.5 - slopes.z_n * SLOPE_STRENGTH;
            }

            let mut height = self.weights[x as usize][y as usize][z as usize];
            y = (y / 1.0).floor() + height;
            while height <= 0.0 && y >= 0.0 {
                y -= 1.0;
                height = self.weights[x as usize][y as usize][z as usize];
                y = (y / 1.0).floor() + height;
            }

            if y < 0.0 {
                stabalized = true;
                reached_edge = true;
                if (self.last_drop_length >= 1) {
                    y = self.last_drop_path[self.last_drop_length - 1].position.y; //Ensure the height is set perfectly to the edge to properly calculate the erosion value;
                }
            }

            if nrof_vertexes < self.last_drop_path.len() {
                self.last_drop_strength[nrof_vertexes] = speed;//* sediment;
                self.last_drop_path[nrof_vertexes] = Vertex {
                    position: Vector3 {
                        x,
                        y,
                        z,
                    },
                    normal: Vector3 { x: 1.0, y: 1.0, z: 1.0 },
                    color: Vector3
                    {
                        x: dx / 2.0 + 0.5,
                        y: slopes.y_p,
                        z: dz / 2.0 + 0.5,
                    },
                };
                nrof_vertexes += 1;
            }

            x += dx;
            z += dz;

            if x < 0.9 || x > GRID_SIZE as f32 - 0.9 || z < 0.1 || z > GRID_SIZE as f32 - 0.9 || (dx.abs() < 0.05 && dz.abs() < 0.05) || nrof_vertexes >= self.last_drop_path.len() {
                if x < 1.1 || x > GRID_SIZE as f32 - 1.1 || z < 1.1 || z > GRID_SIZE as f32 - 1.1 {
                    self.gausian_subtract(x, y, z, -1.0);
                    reached_edge = true;
                }
                stabalized = true;
            }
        }


        let total_path_length = self.calculate_total_path_length(nrof_vertexes);
        if (total_path_length == 0.0) {
            self.last_drop_length = 0;
            return;
        }
        let mut path_length: f32 = 0.0;
        let mut vertex_length: f32 = 0.0;
        let mut vertex_index = 1;
        let length_step = 0.5;
        if (total_path_length.is_nan()) {
            self.last_drop_length = 0;
            return;
        }

        //Proper Ordering:
        // |--------^---------------------^-----------------^-----------------^--------------|
        //          | next_vertex_length  | path_length     | vertex_length   | total_path_length

        while vertex_index < nrof_vertexes {
            if path_length > total_path_length {
                // this means we have overshot the end of the path
                // This can happen when points are very close together
                // Not a problem
                //println!("invalid path length: {}/{}, vertex index: {}/{}", path_length, total_path_length, vertex_index, nrof_vertexes);
                break;
            }

            let strength_factor = if reached_edge && y > GRID_SIZE as f32 * WEAK_EDGE_FACTOR { 2.0 * (total_path_length - path_length) / total_path_length } else { 1.0 * (total_path_length - path_length) / total_path_length - 0.5 };

            //println!("strength factor: {}", strength_factor);


            let vertex_1 = self.last_drop_path[vertex_index].position;
            let vertex_2 = self.last_drop_path[vertex_index + 1].position;
            let next_vertex_length = vertex_length + (vertex_2 - vertex_1).length();

            //assert!(next_vertex_length <= total_path_length + 0.1, "next_vertex_length={}/{} (total: {}, nr: {})", next_vertex_length, total_path_length, path_length, nrof_vertexes);

            if path_length > next_vertex_length {
                vertex_index += 1;
                vertex_length = next_vertex_length;
                continue;
            }

            let dig_position = vertex_interp(path_length, vertex_1, vertex_2, vertex_length, next_vertex_length);
            let dig_strength = ((self.last_drop_strength[vertex_index] + self.last_drop_strength[vertex_index + 1]) * 0.5).min(1.5);

            self.gausian_subtract(dig_position.x, dig_position.y, dig_position.z, -dig_strength * strength_factor);

            path_length += length_step;
            // if path_length < vertex_length {
            //     vertex_index += 1;
            //     vertex_length = next_vertex_length;
            // }
        }

        for i in 0..nrof_vertexes {
            let vertex = self.last_drop_path[i];
            //println!("subtracting {}", self.last_drop_strength[i]);

            self.last_drop_path[i] = Vertex {
                position: Vector3 {
                    x: vertex.position.x / (GRID_SIZE as f32) * 2.0 - 1.0,
                    y: vertex.position.y / (GRID_SIZE as f32) * 2.0 - 1.0 + 0.01,
                    z: vertex.position.z / (GRID_SIZE as f32) * 2.0 - 1.0,
                },
                ..vertex
            }
        }


        self.last_drop_length = nrof_vertexes;
    }

    pub(crate) fn clean(&mut self) {
        let mut info = unsafe_allocate::<[[[GridCellInfo; GRID_SIZE]; GRID_SIZE]; GRID_SIZE]>();
        println!("starting clean...");
        let mut nr_cleaned = 0;

        for y in 0..GRID_SIZE {
            for x in 0..GRID_SIZE {
                for z in 0..GRID_SIZE {
                    if (y == 0) {
                        info[x][y][z].ground_safe = true;
                    } else {
                        if self.weights[x][y][z] > 0.1 && (info[x][y - 1][z].ground_safe ||
                            info[x - 1][y - 1][z].ground_safe ||
                            info[x + 1][y - 1][z].ground_safe ||
                            info[x][y - 1][z - 1].ground_safe ||
                            info[x][y - 1][z + 1].ground_safe) {
                            info[x][y][z].ground_safe = true;
                        } else {
                            info[x][y][z].ground_safe = false;
                        }
                    }
                }
            }
        }

        for y in 0..GRID_SIZE {
            for x in 0..GRID_SIZE {
                for z in 0..GRID_SIZE {
                    if !info[x][y][z].ground_safe && self.weights[x][y][z] > 0.1 {
                        self.weights[x][y][z] = 0.0;
                        nr_cleaned += 1;
                    }
                }
            }
        }

        println!("Cleaned {} vertices", nr_cleaned);
    }
}

impl Drop for Grid {
    fn drop(&mut self) {
        println!("grid deallocate");
    }
}